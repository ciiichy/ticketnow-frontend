import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: []
  },
  mutations: {
    FETCH_USERS(state, users) {
      state.users = users
    }
  },
  actions: {
    fetchUsers({ commit })  {
      const url = process.env.VUE_APP_API_URL + "/api/users";
      fetch(url)
          .then(async response => {
            commit("FETCH_USERS", await response.json());
          })
          .catch(error => {
            console.error("There was an error!", error);
          });
    }
  },
  modules: {
  },
  getters: {
    allUsers: state => {
      return state.users
    }
  }
})
